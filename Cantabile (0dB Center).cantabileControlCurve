{
	// Definitions are a set of function and/or constants

	"definitions":
	{
		// All control curves must provide these two functions 
		// (which must be the inverse of each other else weirdness will ensue).

		//  - Position ranges from 0.0 -> 1.0 and represents the slider position
		//  - Scalar is the amplitude scaling (ie: linear multiplier)

		"positionToScalar(x)": "pow(10, (log(x, 10) * slope - (log(zeroDbPos, 10) * slope)) / 20)",
		"scalarToPosition(x)": "pow(10, (log(x, 10) * 20 + (log(zeroDbPos, 10) * slope)) / slope)",

		// Higher value makes the slope of the curve steeper and gives a higher top gain level
		"slope" : 60,				

		// Position of the 0 dB mark
		"zeroDbPos": "64/127",	
	},

	"uiKinds":
	{
		"horizontalSliderPopup":
		{
			"ticks":
			[
				{ "scalar": 0, 				"label": "-\u221E", "kind": "major" },
				{ "scalar": "fromDb(-45)",	"label": "-45",	"kind": "minor" },
				{ "scalar": "fromDb(-30)",	"label": "-30",	"kind": "minor" },
				{ "scalar": "fromDb(-15)",	"label": "-15",	"kind": "minor" },
				{ "scalar": "fromDb(-9)",	"label": "-9",	"kind": "minor" },
				{ "scalar": "fromDb(-6)",	"label": "-6",	"kind": "minor" },
				{ "scalar": "fromDb(-3)",	"label": "-3",	"kind": "minor" },
				{ "scalar": 1,				"label": "0", "kind": "major" 	},
				{ "scalar": "fromDb(3)",	"label": "+3",	"kind": "minor" },
				{ "scalar": "fromDb(6)",	"label": "+6",	"kind": "minor" },
				{ "scalar": "fromDb(9)",	"label": "+9",	"kind": "minor" },
				{ "scalar": "fromDb(12)",	"label": "+12",	"kind": "minor" },
				{ "scalar": "fromDb(15)",	"label": "+15",	"kind": "minor" },
				{ "scalar": "fromDb(18)",	"label": "+18",	"kind": "minor" },
			]
		},

		"horizontalSlider":
		{
			"ticks":
			[
				{ "scalar": 0, "label": "-\u221E", "kind": "major" },
				{ "scalar": "fromDb(-30)",	"label": "-30",	"kind": "minor" },
				{ "scalar": "fromDb(-15)",	"label": "-15",	"kind": "minor" },
				{ "scalar": "fromDb(-6)",	"label": "-6",	"kind": "minor" },
				{ "scalar": 1,	"label": "0",	"kind": "major" },
				{ "scalar": "fromDb(6)",	"label": "+6",	"kind": "minor" },
				{ "scalar": "fromDb(12)",	"label": "+12",	"kind": "minor" },
				{ "scalar": "fromDb(18)",	"label": "+18",	"kind": "major" },
			]
		},

		"levelMeter":
		{
			"ticks":
			[
				// These get displayed on the level meter as ticks
				{ "scalar": "fromDb(-30)",	"kind": "minor" },
				{ "scalar": "fromDb(-15)",	"kind": "minor" },
				{ "scalar": "fromDb(-6)",	"kind": "minor" },
				{ "scalar": 1,				"kind": "major" },

				{ "scalar": "fromDb(6)",	"kind": "minor" },
				{ "scalar": "fromDb(15)",	"kind": "minor" },

				// These describe how the transition to hot orange works
				{ "scalar": "fromDb(-6)",	"kind": "hotStart" },
				{ "scalar": "1",			"kind": "hotEnd" },

				// Hitting this point turns on the border clip indicator
				{ "scalar": "1",	"kind": "clipIndicator" },
			]
		}
	}
}	