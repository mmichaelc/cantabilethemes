const ControllerType = Object.freeze({'continuous': 1, 'toggle': 2, 'momentary': 3});

const UIType = Object.freeze({'knob': 1, 'slider': 2, 'button': 3});
const UITypeRev = Object.freeze({1: 'knob', 2: 'slider', 3: 'button'});

class MidiController {
    constructor(cc, channel) {
        this.cc = cc;
        this.channel = channel;
        this.binding = null;
    }

    addBinding(binding) {
        this.binding = binding;
    }
}

class UIMidiController extends MidiController {
    constructor(cc, channel, uiType) {
        super(cc, channel);
        this.uiType = uiType;
    }
}

class ControllerBinding {
    constructor(target, level) {
        this.target = target;
        this.level = level;
    }
}