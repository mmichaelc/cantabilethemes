var bindings = {};

emptyBinding = new ControllerBinding('-', 0);

bindings.pianoteq = {
    1: {
        3: new ControllerBinding('Volume', 0),
        4: new ControllerBinding('Condition', 0),
        5: new ControllerBinding('Dynamics', 0),
        6: new ControllerBinding('Key Release', 0),
        8: new ControllerBinding('Mute', 0),
        9: new ControllerBinding('Pedal Noise', 0),
        10: new ControllerBinding('Reverb Duration', 0),
        12: new ControllerBinding('Reverb Mix', 0),
        13: new ControllerBinding('Pre-Delay', 0),
        14: new ControllerBinding('Room Size', 0),
        15: new ControllerBinding('Early Reflections', 0),
        16: new ControllerBinding('Reverb Tone', 0),
        17: new ControllerBinding('Delay Time', 0),
        18: new ControllerBinding('Delay Mix', 0),
        19: new ControllerBinding('Delay Feedback', 0),
        20: new ControllerBinding('Delay Tone', 0),
        29: new ControllerBinding('Reverb', 0),
        35: new ControllerBinding('Delay', 0),
        36: new ControllerBinding('Delay Polarity', 0)
    }
}