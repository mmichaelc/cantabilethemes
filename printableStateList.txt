<html>
<head>
<title>{Title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body { font-family:Arial; margin:20px; font-size:14pt}
td { font-size:14pt }
td:first-child { width: 100px; text-align:center  }
</style>
</head>
<body>
<h1>{Title}</h1>
<table>
{foreach States}
<tr>
<td>{FormattedProgramNumber}</td>
<td>{Name}</td>
</tr>
{/foreach}
</table>
</body>
</html> 